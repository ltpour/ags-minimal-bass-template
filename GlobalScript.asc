//----------------------------------------------------------------------------------------------------
// game_start
//----------------------------------------------------------------------------------------------------
function game_start() 
{
  gQuit.Centre();
  gRestart.Centre();
  gRestore.Centre();
  gSave.Centre();
}

//----------------------------------------------------------------------------------------------------
// repeatedly_execute
//----------------------------------------------------------------------------------------------------
function repeatedly_execute() 
{
}

//----------------------------------------------------------------------------------------------------
// repeatedly_execute_always
//----------------------------------------------------------------------------------------------------
function repeatedly_execute_always() 
{
}

//----------------------------------------------------------------------------------------------------
// on_key_press
//----------------------------------------------------------------------------------------------------
function on_key_press(eKeyCode keycode) 
{
  if (IsGamePaused()) keycode = 0;
  
  // "System Keys"
  if (keycode == eKeyCtrlQ) gQuit.Visible = true; // quit Dialog
  if (keycode == eKeyF5) { // save dialog
    lstSave.FillSaveGameList();
    lstSave.SelectedIndex = 0;
    if(lstSave.SelectedIndex != -1) {
      txtSave.Text = lstSave.Items[lstSave.SelectedIndex];
    }
    gSave.Visible = true;
  }
  if (keycode == eKeyF7) { // restore dialog
    lstRestore.FillSaveGameList();
    lstRestore.SelectedIndex = 0;
    gRestore.Visible = true;
  }
  if (keycode == eKeyF9) gRestart.Visible = true; // restart dialog
  if (keycode == eKeyF12) SaveScreenShot("scrnshot.pcx"); // screenshot
  
  // Debugger Keys
  if (keycode == eKeyCtrlS) Debug(0,0); // Ctrl-S, give all inventory
  if (keycode == eKeyCtrlV) Debug(1,0); // Ctrl-V, version
  if (keycode == eKeyCtrlA) Debug(2,0); // Ctrl-A, show walkable areas
  if (keycode == eKeyCtrlX) Debug(3,0); // Ctrl-X, teleport to room
}

//----------------------------------------------------------------------------------------------------
// on_mouse_click
//----------------------------------------------------------------------------------------------------
function on_mouse_click(MouseButton button)
{
	// all mouse clicks are handled in TwoClickHandler.asc!
}

//----------------------------------------------------------------------------------------------------
// on_event
//----------------------------------------------------------------------------------------------------
function on_event (EventType event, int data) 
{
}

//----------------------------------------------------------------------------------------------------
// unhandled_event
//----------------------------------------------------------------------------------------------------
function unhandled_event (int what, int type) 
{
	if (what == 1) // Unhandled events for HOTSPOTS
	{
		if (type == 1) // look
		{
			player.Say("I see nothing special about it.");
		}
		if (type == 2) // interact
		{
			player.Say("I can't do anything with it.");
		}
		if (type == 3) // use inv on
		{
			player.Say("That won't do anything.");
		}
	}
	if (what == 2) // Unhandled events for OBJECTS
	{
		if (type == 0) // look
		{
			player.Say("Looks alright.");
		}
		if (type == 1) // interact
		{
			player.Say("I don't want to have it.");
		}
		if (type == 3) // use inv on
		{
			player.Say("That's a funny idea.");
		}
	}
	if (what == 3) // Unhandled events for CHARACTERS
	{
		if (type == 0) // look
		{
			player.Say("Hm.");
		}
		if (type == 1) // interact
		{
			player.Say("Got nothing to say.");
		}
		if (type == 3) // use inv on
		{
			player.Say("I don't think I should give that away.");
		}
	}
	if (what == 5) // Unhandled events for INVENTORY ITEMS
	{
		if (type == 0) // look
		{
			player.Say("It's just some junk in my inventory.");
		}
		if (type == 1) // interact
		{
			player.Say("Er, no?");
		}
		if (type == 3) // use inv on
		{
			player.Say("That's ridiculous.");
		}
	}
}

//----------------------------------------------------------------------------------------------------
// dialog_request
//----------------------------------------------------------------------------------------------------
function dialog_request(int param) 
{
}

//----------------------------------------------------------------------------------------------------
// gInventoryBar
//----------------------------------------------------------------------------------------------------
function btnInvScrollLeft_OnClick(GUIControl *control, MouseButton button)
{
	InventoryWindow1.ScrollDown();
}

function btnInvScrollRight_OnClick(GUIControl *control, MouseButton button)
{
	InventoryWindow1.ScrollUp();
}

function btnQuitDialog_OnClick(GUIControl *control, MouseButton button)
{
	gQuit.Visible = true;
}



// TEST INVENTORY ITEMS

// The "beef jerky" inventory item
function iBeefJerky_Look()
{
	player.Say("Just jerky enough.");
}

// The "blue cup" inventory item
function iBlueCup_Look()
{
	player.Say("You sure see them a lot these days!");
}

function iBlueCup_UseInv()
{
	player.Say("It's for holding coffee, or tea, or MAYBE cocoa. Nothing else.");
}

// The "book" inventory item

// The "pan pipes" inventory item: This one is an example of an item that is used directly
function iPipes_Look()
{
	player.Say("I just started to learn how to play them.");
}

function iPipes_Interact()
{
	player.Say("Pfeeeeeeeehhhhppppphrrrrrfff...");
}


//----------------------------------------------------------------------------------------------------
// gQuit
//----------------------------------------------------------------------------------------------------
function btnQuit_OnClick(GUIControl *control, MouseButton button)
{
  QuitGame(0);
}

function btnCancelQuit_OnClick(GUIControl *control, MouseButton button)
{
  gQuit.Visible = false;
}

//----------------------------------------------------------------------------------------------------
// gRestart
//----------------------------------------------------------------------------------------------------
function btnRestart_OnClick(GUIControl *control, MouseButton button)
{
  RestartGame();
}

function btnCancelRestart_OnClick(GUIControl *control, MouseButton button)
{
  gRestart.Visible = false;
}

//----------------------------------------------------------------------------------------------------
// gSave
//----------------------------------------------------------------------------------------------------
function btnSave_OnClick(GUIControl *control, MouseButton button)
{
  int totalSaves = lstSave.ItemCount;
  String saveName = txtSave.Text;
  
  // make sure that a name has been entered
  if(saveName == "") {
    Display("Please enter a name for your saved game.");
    return;
  }
  
  // if no saves exist, save to slot 1
  if(lstSave.SelectedIndex == -1) {
    gSave.Visible = false;
    SaveGameSlot(1, saveName);
  } else {
    // if name matches existing save, overwrite it
    int checkSave = 0;
    while(checkSave != totalSaves) {
      if(saveName == lstSave.Items[checkSave]) {
        gSave.Visible = false;
        SaveGameSlot(savegameindex[checkSave], saveName);
        return;
      }
      checkSave++;
    }
    // no match, save to a new slot
    if(totalSaves < 20) {
      gSave.Visible = false;
      SaveGameSlot(totalSaves+1, saveName);
    } else {
      Display("The maximum number of saved games has been reached; overwrite or delete some old ones.");
    }
  }
}

function btnDeleteSave_OnClick(GUIControl *control, MouseButton button)
{
  if(lstSave.SelectedIndex == -1) {
    Display("There are no saved games to delete.");    
  } else {
    DeleteSaveSlot(savegameindex[lstSave.SelectedIndex]);
    lstSave.FillSaveGameList();
  }
}

function btnCancelSave_OnClick(GUIControl *control, MouseButton button)
{
  gSave.Visible = false;
}

function txtSave_OnActivate(GUIControl *control)
{
  txtSave.Text = lstSave.Items[lstSave.SelectedIndex];
}

function lstSave_OnSelectionChanged(GUIControl *control)
{
  txtSave.Text = lstSave.Items[lstSave.SelectedIndex];
}

//----------------------------------------------------------------------------------------------------
// gRestore
//----------------------------------------------------------------------------------------------------
function btnRestore_OnClick(GUIControl *control, MouseButton button)
{
  if(lstRestore.SelectedIndex == -1) {
    Display("There is no saved game to load.");
  } else {
    RestoreGameSlot(savegameindex[lstRestore.SelectedIndex]);
    gRestore.Visible = false;
  }
}

function btnCancelRestore_OnClick(GUIControl *control, MouseButton button)
{
  gRestore.Visible = false;
}
