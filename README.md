# AGS Minimal BASS Template #

A template for Adventure Game Studio 3.4.*. Based on Ghost's Lightweight BASS Template v2.0.

The fonts included are ROTORcap Neue by David Lindecrantz and David Device by David Roberge.
